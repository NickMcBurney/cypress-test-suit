/// <reference types="Cypress" />

context('Actions', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8080/')
    })
    /*describe('Loading', () => {
        it('.load() - first screen', () => {
            // check logo
            cy.get('.header img').should('be.visible');
            // check progress bar
            cy.get('.progress-bar').should('be.visible');
            // check first question
            cy.get('.question:first-of-type').should('be.visible');
            cy.get('input[name=title]').should('be.visible');
        }) 
    })*/
    describe('Happy Path', () => {
        it('.form() - happy path', () => {
            //////////////////////////////
            //////////////////////////////
            // ABOUT YOU
            //////////////////////////////
            //////////////////////////////

            //////////////////////////////
            // title
            //////////////////////////////
            cy.get('label[for=title_Mr]').should('be.visible')
            .click()
   
            //////////////////////////////
            // first name
            //////////////////////////////
            cy.get('#fullname1').should('be.visible')
            .type('Richard')
            .blur()
            //.parent().should('have.class', 'good') // no longer needed since first/second name share same parent element

            //////////////////////////////
            // second name
            //////////////////////////////
            cy.get('#fullname2').should('be.visible')
            .type('Carlisle')
            .blur()
            .parent().parent().should('have.class', 'good')

            //////////////////////////////
            // go to next slide
            //////////////////////////////
            cy.get('.VueCarousel-slide-active .btn-next').should('be.visible')
            .click()

            //////////////////////////////
            // date of birth
            //////////////////////////////
            cy.get('.VueCarousel-slide-active .three-col-input input:first-of-type').should('be.visible')
            .type('12')
            cy.get('.VueCarousel-slide-active .three-col-input input:nth-of-type(2n)').should('be.visible')
            .type('03')
            cy.get('.VueCarousel-slide-active .three-col-input input:last-of-type').should('be.visible')
            .type('1979')
            .blur()
            .parent().parent().should('have.class', 'good')

            //////////////////////////////
            // go to next slide
            //////////////////////////////
            cy.get('.VueCarousel-slide-active .btn-next').should('be.visible')
            .click()


            //////////////////////////////
            //////////////////////////////
            // ADDRESS BLOCK
            //////////////////////////////
            //////////////////////////////

            //////////////////////////////
            // live in the UK
            //////////////////////////////
            cy.get('label[for=liveUK_true]').should('be.visible')
            .click()

            //////////////////////////////
            // find address
            //////////////////////////////
            // type postcode
            cy.get('input[name=house_number1]').should('be.visible')
            .type('1')
            .blur()

            cy.get('input[name=postcode1]').should('be.visible')
            .type('M33 4rf')
            //.blur()
            //.parent().should('have.class', 'good')

            // click find address button
            .parent().next().next('.findAddress').click()
            //cy.get('.findAddress').click()
            // check an address was returned, and class added
            cy.get('.selected-address').should('have.class', 'good')
            // get the years label
            cy.contains('How many years have you lived at this address?').next('input').type('3')
            

        })
        /*it('.form() - enter date of birth', () => {
            cy.get('.question:first-of-type').should('be.visible');

            cy.get('input[name=title]').should('be.visible');
        })*/
    })
    /*
    it('.trigger() - trigger an event on a DOM element', () => {
        // https://on.cypress.io/trigger
    
        // To interact with a range input (slider)
        // we need to set its value & trigger the
        // event to signal it changed
    
        // Here, we invoke jQuery's val() method to set
        // the value and trigger the 'change' event
        cy.get('.trigger-input-range')
          .invoke('val', 25)
          .trigger('change')
          .get('input[type=range]').siblings('p')
          .should('have.text', '25')
      })
      */
    })